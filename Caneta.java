public class Caneta{

// Public para que outras partes do software possa acessar-la

	private String cor;
	private String material;
	private String tampa;

//Atributos devem ser privados;

	public void setCor(String cor){
	 	this.cor = cor;
	}

	public  String getCor(){				
	return cor;
	}
	
	public void setMaterial(String material){
                this.material = material;
        }

        public  String getMaterial(){
        return material;
        }
	
	public void setTampa(String tampa){
                this.tampa = tampa;
        }

        public String getTampa(){
        return tampa;
        }

}


