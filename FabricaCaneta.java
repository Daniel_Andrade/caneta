import java.util.*;
public class FabricaCaneta{

	public static void main (String arg[]){
		
		Scanner ler = new Scanner(System.in);
		Caneta umaCaneta = new Caneta(); 
		
		System.out.println("Digite a cor da caneta: ");
		umaCaneta.setCor(ler.nextLine());
		System.out.println("Digite o material: ");
		umaCaneta.setMaterial(ler.nextLine());
		System.out.println("Tem tampa?: ");
		umaCaneta.setTampa(ler.nextLine());

		System.out.println("Cor da caneta: "+umaCaneta.getCor()+"\n"+"Material: "+umaCaneta.getMaterial()+"\n"+"Tem Tampa?: "+umaCaneta.getTampa());
	}
}

